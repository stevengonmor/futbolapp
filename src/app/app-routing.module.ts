import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: '',
    redirectTo: 'matches',
    pathMatch: 'full',
  },
  {
    path: 'matches',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./matches/matches.module').then((m) => m.MatchesPageModule),
      },
      {
        path: ':matchId',
        loadChildren: () =>
          import('./matches/detail/detail.module').then(
            (m) => m.DetailPageModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
