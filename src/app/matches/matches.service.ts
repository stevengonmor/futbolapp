/* eslint-disable arrow-body-style */
import { Injectable } from '@angular/core';
import { Match } from './matches.model';

@Injectable({
  providedIn: 'root',
})
export class MatchesService {
  private matches: Match[] = [
    {
      id: 'match1',
      teamOne: 'Argentina',
      teamTwo: 'Brazil',
      score: '2 - 1',
      events: [
        '[23:00] El jugador 1 de Brazil anoto un gol',
        '[30:00] El jugador 7 de Brazil recibio una tarjeta roja',
        '[46:00] El jugador 2 de Argentina anoto un gol',
        '[75:00] El jugador 10 de Argentina anoto un gol',
      ],
      teamOneImg: '/assets/img/argentina.png',
      teamTwoImg: '/assets/img/brazil.png',
    },
    {
      id: 'match2',
      teamOne: 'Estados Unidos',
      teamTwo: 'Costa Rica',
      score: '2 - 2',
      events: [
        '[17:00] El jugador 7 de Estados Unidos anoto un gol',
        '[40:00] El jugador 8 de Estados Unidos anoto un gol',
        '[43:00] El jugador 10 de Estados Unidos recibio una tarjeta amarilla',
        '[60:00] El jugador 10 de Costa Rica recibio una tarjeta roja',
        '[65:00] El jugador 2 de Costa Rica anoto un gol',
        '[85:00] El jugador 5 de Costa Rica anoto un gol',
      ],
      teamOneImg: '/assets/img/unitedStates.png',
      teamTwoImg: '/assets/img/costaRica.png',
    },
    {
      id: 'match3',
      teamOne: 'Uruguay',
      teamTwo: 'Colombia',
      score: '1 - 3',
      events: [
        '[33:00] El jugador 1 de Colombia anoto un gol',
        '[43:00] El jugador 5 de Colombia anoto un gol',
        '[65:00] El jugador 8 de Colombia anoto un gol',
        '[68:00] El jugador 2 de Uruguay anoto un gol',
        '[78:00] El jugador 7 de Uruguay recibio una tarjeta roja',
        '[80:00] El jugador 4 de Colombia recibio una tarjeta roja',
      ],
      teamOneImg: '/assets/img/uruguay.png',
      teamTwoImg: '/assets/img/colombia.png',
    },
  ];
  constructor() {}
  getAll() {
    return [...this.matches];
  }
  getMatch(matchId: string) {
    return {
      ...this.matches.find((match) => {
        return matchId === match.id;
      }),
    };
  }
  deleteMatch(matchId: string) {
    this.matches = this.matches.filter((match) => {
      return match.id !== matchId;
    });
  }
}
