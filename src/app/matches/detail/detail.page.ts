import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, RouterLinkDelegate } from '@ionic/angular';
import { Match } from '../matches.model';
import { MatchesService } from '../matches.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  match: Match;
  showItem: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private matchesService: MatchesService,
    private router: Router,
    private alertCtrl: AlertController
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap) => {
      if (!paramMap.has('matchId')) {
        return;
      }
      const matchId = paramMap.get('matchId');
      this.match = this.matchesService.getMatch(matchId);
    });
  }
  delete() {
    this.alertCtrl
      .create({
        header: 'Borrar',
        message: 'Seguro que desea eliminar este partido?',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
          },
          {
            text: 'Borrar',
            handler: () => {
              this.matchesService.deleteMatch(this.match.id);
              this.router.navigate(['/matches']);
            },
          },
        ],
      })
      .then((alertElement) => {
        alertElement.present();
      });
  }

  toggleCheckbox() {
    this.showItem = !this.showItem;
  }
}
