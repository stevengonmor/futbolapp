import { Component, OnInit } from '@angular/core';
import { Match } from './matches.model';
import { MatchesService } from './matches.service';

@Component({
  selector: 'app-matches',
  templateUrl: './matches.page.html',
  styleUrls: ['./matches.page.scss'],
})
export class MatchesPage implements OnInit {
  matches: Match[];
  constructor(private matchesService: MatchesService) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.matches = this.matchesService.getAll();
  }
}
