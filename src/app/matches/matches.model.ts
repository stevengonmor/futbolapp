export interface Match {
  id: string;
  teamOne: string;
  teamTwo: string;
  score: string;
  events: string[];
  teamOneImg: string;
  teamTwoImg: string;
}
